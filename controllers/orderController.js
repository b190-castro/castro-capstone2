const Order = require("../models/Order.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");


module.exports.orderProducts = async (data, id) => {
	let productIds = data.products;
	let productDetails = [];
	let totalAmount = 0;
	if (productIds.length > 0){
		for (let i = 0; i < productIds.length; i++) {
			await Product.findById(productIds[i]).then(result => {
				totalAmount = totalAmount + result.price
				productDetails.push(result)
			})
		}
	}else{
		return "You cannot create order without Product Id provided"
	};
	
	let newOrder = new Order ({
		totalAmount: totalAmount,
		buyerId: id,
		products: productDetails
	})
	return newOrder.save().then( async (order, error) => {
		console.log(order);
		if (error) {
			return "Order creation failed."
		} else {
			await User.findById({_id:id}).then(result =>{
				result.orderIds.push(order)
				result.save()
			})
			return "Order successfully created!"
		}
	})
}

// Retrieve authenticated user's order
module.exports.userOrder = (id) => {
	return Order.find({buyerId:id}).then(result => {
		return result;
	})
};

// Retrieve all orders (Admin only)
module.exports.allOrders = () => {
	return Order.find({}).then(result => {
		return result;
	})
};