const Product = require("../models/Product.js");

// Create Product (Admin only)
module.exports.addProduct = (data) => {
		let newProduct = new Product({
			name : data.name,
			description : data.description,
			price : data.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return "Product registration failed.";
			} else {
				return "Product successfully created!";
			};
		});
};

// Retrieve all active products
module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result =>{
		return result
	})
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
};

// Update product information
module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product,err) =>{
		if (err) {
			return "Product update failed.";
		}else{
			return "You have successfully update product information!";
		}
	})
}

// Archive product
module.exports.archiveProduct = (reqParams) => {
	return Product.findByIdAndUpdate(reqParams.productId, {isActive: false}).then(result => {
		if (result == null){
			return "Product archiving failed.";
		}else{
			return "You have successfully updated product status!";
		}
	})
}
