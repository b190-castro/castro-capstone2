const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// User Registration with Email Validation
module.exports.registerUser = (reqBody) => {
	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0){
			return "User is already registered."
		} else {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				// to allow the registration of an admin user
				isAdmin: ((reqBody.isAdmin != null) ? reqBody.isAdmin : false)
			})
			return newUser.save().then((user, error) =>{
				if (error) {
					return "Error: " + error;
				}else{
					return "You have successfully registered!";
				}
			})
		}
	})	
};

// User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null){
			return "Invalid user credentials.";
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return "Password is incorrect.";
			}
		}
	})
}

// Set user as admin (Admin only)
module.exports.changeUser = (reqParams) => {
	return User.findByIdAndUpdate(reqParams.userId, {isAdmin: true}).then(result => {
		if (result == null){
			return "User not found.";
		}else{
			return "You have successfully updated user credentials.";
		}
	})
}

// retrieve all user (Admin only)
module.exports.allUser = () => {
	return User.find({}).then(result => {
		return result;
	})
};