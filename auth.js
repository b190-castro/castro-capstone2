const jwt = require("jsonwebtoken");
const secret = "BulbasaurCharmanderSquirtle";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};

module.exports.verifyToken = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return res.send({auth: "Bearer token is invalid"});
			}else{
				next();
			}
		})
	}else{
		return res.send({auth:"verification failed"});
	}
};

module.exports.decodeToken = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}else{
				return jwt.decode(token, { complete : true }).payload;
			}
		})
	}else{
		return null;
	};
};