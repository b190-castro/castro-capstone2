const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");

// Non-admin User checkout (create order)
router.post("/add-to-bag", auth.verifyToken, ( req, res ) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === true){
		res.send("Admin User cannot create on order.")
	} else {
		orderController.orderProducts(req.body, auth.decodeToken(req.headers.authorization).id).then(resultFromController => res.send(resultFromController));
	}
});

// retrieve authenticated user's order
router.get("/get-order", auth.verifyToken, (req, res) => {
		orderController.userOrder(auth.decodeToken(req.headers.authorization).id).then(resultFromController => res.send(resultFromController));
	}
)

// retrieve all orders
router.get("/all-orders", auth.verifyToken, ( req, res) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === false){
		res.send("User is not authorized to retrieve all orders.")
	} else {
		orderController.allOrders().then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router