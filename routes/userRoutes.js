const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// User Authentication
router.post("/login", ( req, res ) => {
	userController.loginUser(req.body).then( resultFromController => res.send(resultFromController));
});

// Set user as admin (Admin only)
router.put("/:userId/setAsAdmin", auth.verifyToken, (req,res) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === false){
		res.send("Current User is not allowed to change other user's credentials.")
	} else {
	userController.changeUser(req.params).then(resultFromController => res.send(resultFromController));
	}
});

// Retrieve all user
router.get("/all-user", auth.verifyToken, ( req, res) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === false){
		res.send("User is not authorized to process request.")
	} else {
		userController.allUser().then(resultFromController => res.send(resultFromController));
	}
})

module.exports = router;