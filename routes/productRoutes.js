const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Create Product (Admin)
router.post("/add-product", auth.verifyToken, (req, res) => {
	if(auth.decodeToken(req.headers.authorization).isAdmin === false) {
		res.send("You don't have permission to add a product.");
	} else {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});

// Retrieve all active products
router.get("/all-active", (req,res) => {
	productController.getActiveProducts().then(resultFromController => {
		res.send(resultFromController)
	})
});

// Retrieve single product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update product information
router.put('/:productId', auth.verifyToken, (req, res) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === false){
		res.send("User is not allowed to change product status.")
	} else {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
});

// Archive product
router.put('/:productId/archive', auth.verifyToken, (req, res) => {
	if (auth.decodeToken(req.headers.authorization).isAdmin === false){
		res.send("User is not allowed to archive product.")
	} else {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;