const mongoose = require("mongoose");

let orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Total amount must be indicated"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	buyerId: {
		type: String,
		required: [true, "Buyer id is required"]
	},
	products: [
		{
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			description: {
				type: String,
				required: [true, "Product description is required"]
			},
			price: {
				type: Number,
				required: [true, "Product price is required"]
			},
			isActive: {
				type: Boolean,
				default: true
			},
			createdOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Order", orderSchema);