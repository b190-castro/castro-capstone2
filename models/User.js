const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isActive: {
		type: Boolean,
		default: true
	},
	orderIds: [
		{
			totalAmount: {
					type: Number,
					required: [true, "Total amount must be indicated"]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				},
				buyerId: {
					type: String,
					required: [true, "Buyer id is required"]
				},
				products: [
					{
						name: {
							type: String,
							required: [true, "Product name is required"]
						},
						description: {
							type: String,
							required: [true, "Product description is required"]
						},
						price: {
							type: Number,
							required: [true, "Product price is required"]
						},
						isActive: {
							type: Boolean,
							default: true
						},
						createdOn: {
							type: Date,
							default: new Date()
						}
					}
				]
		}
	]
});

module.exports = mongoose.model("User", userSchema);